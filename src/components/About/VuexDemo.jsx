import { useStore } from 'vuex'

export default {
  setup() {
    const store = useStore()
    function commit(mutation) {
      store.commit(mutation)
    }
    function dispatch(mutation) {
      store.dispatch(mutation)
    }
    return () => (
      <>
        <button onClick={() => commit('countIncrement')}>count++</button>
        <button onClick={() => commit('countDecrement')}>count--</button><br />
        <button onClick={() => dispatch('countIncrementDelay')}>count++ Delay</button><br />
        count: {store.state.count}<br />
        {store.state.fibonacci ? 'fibonacci: ' + store.state.fibonacci : 'uncomputed' }<br />
      </>
    )
  }
}