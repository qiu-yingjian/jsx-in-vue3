import { KeepAlive, reactive, ref, toRefs, watch } from 'vue';

export default {
  setup() {
    return () => (
      <>
        <Input />
        <Fibonacci />
        <Arrs />
      </>
    )
  }
}

const Input = {
  setup() {
    const input = ref()
    return () => (
      <>
        <input type='text' v-model={input.value} />
        <h1>{input.value ? input.value : '没有输入'}</h1>
      </>
    )
  }
}

const Arrs = {
  setup() {
    const list = reactive({
      arr: [-1, -2, -3]
    })
    let count = 0
    watch(list.arr,(newVal, preVal) => {
        console.log(newVal) 
      },{ deep: true }
    )
    function pop() {
      list.arr.pop()
    }
    function push() {
      list.arr.push(count++)
    }
    function unshift() {
      list.arr.unshift(count++)
    }
    function shift() {
      list.arr.splice(0, 1)
    }
    return () => (
      <>
        <button onClick={() => pop()}>pop</button>
        <button onClick={() => push()}>push</button>
        <button onClick={() => unshift()}>unshift</button>
        <button onClick={() => shift()}>shift</button><br />
        {list.arr}
        <Arr arr={list.arr}/>
      </>
    )
  }
}

const Arr = {
  props: ['arr'],
  setup(props) {
    const { arr } = toRefs(props) // props的解构方式
    return () => (
      <>
        {
          arr.value.map((li, i) => (
            <li style={{ color: !(li % 2) ? 'red' : 'black' }} key={li}>index:{i} item:{li}</li>
          ))
        }
      </>
    )
  }
}

// 与react中相似的低性能代码,区别是vue自动完成了优化, 原因: react的useState在每次渲染时都会重新计算
const Fibonacci = {
  setup() {
    const fibonacci = (n) => {
      if (n <= 0) return 0
      if (n === 1 || n === 2) return 1
      return fibonacci(n - 2) + fibonacci(n - 1)
    }
    const count = ref(40)
    const f = ref(fibonacci(count.value))
    watch(count, (newVal, preVal) => {
      f.value = fibonacci(newVal)
    })
    return () => (
      <>
        <button onClick={() => count.value += 1 }>count++</button><br />
        count:{count.value} fibonacci:{f.value}
        <br />
      </>
    )
  }
}