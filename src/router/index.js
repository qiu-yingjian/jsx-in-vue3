import { createRouter, createWebHashHistory } from 'vue-router'

const routes = [
  {
    path: '/',
    name: 'home',
    component: () => import('../views/HomeView'),
    meta: {
      keepAlive: true,
    }
  },
  {
    path: '/about',
    name: 'about',
    component: () => import('../views/AboutView'),
    meta: {
      keepAlive: false,
    }
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router