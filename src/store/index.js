import { createStore } from 'vuex'

export default createStore({
  state: {
    count: 40,
    fibonacci: 0,
  },
  getters: {
  },
  mutations: {
    countIncrement (state) { 
      state.count++
      this.commit('getFibonacci')
    },
    countDecrement (state) { 
      state.count--
      this.commit('getFibonacci')
    },
    getFibonacci (state) {
      function get(n) {
        if (n <= 0) return 0
        if (n === 1 || n === 2) return 1
        return get(n - 2) + get(n - 1)
      }
      state.fibonacci = get(state.count)
    }
  },
  actions: {
    countIncrementDelay (context) { 
      setTimeout(() => {
        context.commit('countIncrement')
      }, 1000)
    }
  }
})
