import HelloWorld from '../components/HelloWorld'
import VueLogo from '../assets/logo.png'

export default {
  setup() {
    return () => (
      <div className='home'>
        <h1>This is a home page</h1>
        <img alt="Vuelogo" src={VueLogo} /><br />
        <HelloWorld />
      </div>
    )
  }
}