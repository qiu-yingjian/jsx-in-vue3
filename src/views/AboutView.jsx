import VuexDemo from '@/components/About/VuexDemo'
import ElementDemo from '@/components/About/ElementDemo'
import SlotDemo from '@/components/About/SlotDemo.vue'

export default {
  setup() {
    
    return () => (
      <div className='about'>
        <h1>This is an about page</h1>
        <hr />
        <VuexDemo />
        <hr />
        <ElementDemo />
        <hr />
        {/* 两种向原始vue组件中passing插槽的方式 */}
        <SlotDemo>
          {{
            default: () => <h1>default slot</h1>,
            foo: () => <div>foo</div>,
            bar: () => (
              <>
                <span>one</span>
                <span>two</span>
              </>
            )
          }}
        </SlotDemo>
        {/* 个人推荐使用v-slots */}
        <SlotDemo v-slots={{
          default: () => <h1>default slot</h1>,
          foo: () => <div>foo</div>,
          bar: () => (
            <>
              <span>one</span>
              <span>two</span>
            </>
          )
        }} />
      </div>
    )
  }
}