import router from './router';
import './App.less';
import { KeepAlive, reactive, ref, watch } from "vue";

export default {
  setup() {
    const routers = reactive(router)
    const keepAliveState = ref(false)
    watch(routers, (newValue, oldValue)=>{
      if (newValue.currentRoute.meta.keepAlive) {
        keepAliveState.value = true
      }
    })
    return () => (
      <>
        <Link.Path to={'/'} children={'home'}/>
        <Link.Name to={'about'} children={<button>about</button>} />
        <router-view
          v-slots={{
            default: (scope) => {
              // console.log(scope)
              return keepAliveState.value ? <KeepAlive>{scope.Component}</KeepAlive> : <>{scope.Component}</>
            }
          }}
        />
      </>
    )
  }
}

const Link = {
  Path: {
    props: ['to', 'children'],
    setup({ to, children }) {
      function pathTo(path) {
        router.push({ path: path })
      }
      return () => (
        <span className='link' onClick={() => pathTo(to)}>{children ? children : 'link'}</span>
      )
    }
  },
  Name: {
    props: ['to', 'children'],
    setup({ to, children }) {
      function nameTo(name) {
        router.push({ name: name })
      }
      return () => (
        <span className='link' onClick={() => nameTo(to)}>{children ? children : 'link'}</span>
      )
    }
  },
}